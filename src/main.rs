use std::io;

fn main() {
    loop {
        println!("Please input a temperature in Fahrenheit.");

        let mut temp = String::new();

        io::stdin().read_line(&mut temp)
            .expect("Failed to read line");

        let temp: f64 = match temp.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        let temp = (temp - 32.0) * 5.0 / 9.0;
        println!("That is {} celsius", temp);
        break;
    }
}
